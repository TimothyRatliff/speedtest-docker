FROM debian:stable-slim
LABEL MAINTAINER chevdor@gmail.com

ENV CRON='0 * * * *'
ENV DB=speedtests
ENV INFLUXDB_HOST=localhost
ENV INFLUXDB_PORT=8086
ENV TARGET=/etc/cron.d/speedtest-cron
ENV SERVER=

RUN apt-get update && \
    apt-get install -y \
        gnupg1 apt-transport-https dirmngr curl jq bc && \ 
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61 && \
    echo "deb https://ookla.bintray.com/debian generic main" | tee  /etc/apt/sources.list.d/speedtest.list && \
    apt-get update && \
    apt-get install -y speedtest cron && \
    rm -rf /var/lib/apt/lists/*

ADD setup.sh /usr/local/bin
ADD run.sh /usr/local/bin
ADD start.sh /usr/local/bin
RUN touch /var/log/cron.log

CMD ["/usr/local/bin/start.sh"]