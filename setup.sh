#!/usr/bin/env bash

LOCK=/var/lock/speedtest

# if [ -f "$LOCK" ]; then
#     echo "$LOCK already exists."
# else
    # Setup CRON task
    CRON_TASK="$CRON root /usr/local/bin/run.sh"
    echo "Init cron task with '$CRON_TASK' in '$TARGET'"
    rm -rf $TARGET
    touch $TARGET 
    chmod 0644 $TARGET
    echo -e "$CRON_TASK > /var/log/cron.log 2>&1" >> $TARGET

    # Add ENV to the profile that is sourced when the cron task runs
    echo "export DB=$DB" > /etc/profile
    echo "export INFLUXDB_HOST=$INFLUXDB_HOST" >> /etc/profile
    echo "export INFLUXDB_PORT=$INFLUXDB_PORT" >> /etc/profile
    echo "export SERVER=$SERVER" >> /etc/profile

    # Show some infos about the speedtest
    speedtest --accept-license --accept-gdpr --version
    speedtest --accept-license --accept-gdpr -L

    # Create the influxdb database in case it does not exist
    curl -S -s -XPOST "http://$INFLUXDB_HOST:$INFLUXDB_PORT/query" --data-urlencode "q=CREATE DATABASE $DB"

    # We are done
    echo "Init finished, the first test will run according to your cron task: $TARGET:" 
    cat $TARGET 
    touch $LOCK
# fi
